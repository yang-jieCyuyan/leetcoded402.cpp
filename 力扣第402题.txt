class Solution {
public:
    string removeKdigits(string num, int k) {
        string ret;
        string s;
        for(int i=0;i<num.size();i++)
        {
            if(ret.empty()||num[i]>=ret.back()||k==0)
            {
                ret+=num[i];
            }
            else
            {
                while(!ret.empty()&&k!=0&&num[i]<ret.back())
                {
                    k--;
                    ret.pop_back();
                }
                ret+=num[i];
            }
        }
        int flag=0;
        for(int i=k;i>0;i--)
        {
            ret.pop_back();
        }
        for(int i=0;i<ret.size();i++)
        {
            if(ret[i]!='0'||flag==1)
            {
                s+=ret[i];
                flag=1;
            }
        }
        if(s=="")
           return "0";
        return s;
    }
};